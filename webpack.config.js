var path = require('path');

module.exports = {
  entry: './js/index.jsx',
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'static/js/')
  },
  module: {
    loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          query: {
            presets: ['es2015', 'react']
          }
        }
    ]
  }
};