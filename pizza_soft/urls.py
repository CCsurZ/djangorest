from django.conf.urls import url
from django.contrib import admin

from pizza_soft.main_site import views as rest_views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
                  url(r'^api/pizza/', rest_views.get_pizza),
                  url(r'^api/order/', rest_views.get_order),
                  url(r'^api/addpizza/(?P<id>[0-9]+)', rest_views.add_pizza),
                  url(r'^api/removepizza/(?P<id>[0-9]+)', rest_views.remove_pizza),
                  url(r'^api/checkout/', rest_views.checkout),
                  url(r'^api/droporder/', rest_views.drop_order),
                  url(r'^admin/', admin.site.urls),
                  url(r'^$', rest_views.index),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
