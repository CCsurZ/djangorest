from rest_framework import serializers

from pizza_soft.main_site.models import Pizza, Ingredient, Expense, Order, OrderToPizza


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('id', 'name',)


class PizzaSerializer(serializers.ModelSerializer):
    ingredient = IngredientSerializer(many=True, read_only=True)
    image = serializers.ImageField(use_url=False)

    class Meta:
        model = Pizza
        # fields = '__all__'
        fields = ('id', 'name', 'price', 'image', 'cooking_time', 'ingredient')


class ExpenseSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='ingredient.id')
    name = serializers.ReadOnlyField(source='ingredient.name')

    class Meta:
        model = Expense
        fields = ('id', 'name', 'amount', 'pizza_id')


class OrderToPizzaSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField(source='pizza.id')
    name = serializers.ReadOnlyField(source='pizza.name')
    price = serializers.ReadOnlyField(source='pizza.price')

    class Meta:
        model = OrderToPizza
        fields = ('id', 'name', 'price', 'amount')


class OrderSerializer(serializers.ModelSerializer):
    pizza = OrderToPizzaSerializer(source='ordertopizza_set', many=True)

    class Meta:
        model = Order
        fields = ('id', 'price', 'pizza', 'address', 'status')
