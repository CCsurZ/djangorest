from django.contrib import admin

from .models import *


class IngredientInline(admin.TabularInline):
    model = Expense


class PizzaAdmin(admin.ModelAdmin):
    inlines = (IngredientInline,)


admin.site.register(Ingredient)
admin.site.register(Pizza, PizzaAdmin)
admin.site.register(Order)
admin.site.register(Expense)
admin.site.register(OrderToPizza)
