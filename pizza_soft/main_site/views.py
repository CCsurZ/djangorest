import json

from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from rest_framework import status
from rest_framework.renderers import JSONRenderer

from pizza_soft.main_site.models import Pizza, Order, OrderToPizza
from pizza_soft.main_site.serializers import PizzaSerializer, OrderSerializer


def auth(fn):
    def wrapped(request, *args, **kwargs):
        if request.session.session_key is None:
            request.session.create()
        if not Order.objects.filter(session=request.session.create_model_instance([])).exists():
            request.session.modify = True
            order = Order(session=request.session.create_model_instance([]))
            order.save()
        return fn(request, *args, **kwargs)

    return wrapped


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super().__init__(content, **kwargs)


def get_pizza(request):
    pizzas = []
    for pizza in Pizza.objects.all():
        if pizza.ingredient.filter(expense__amount__lte=F('count')).count() == pizza.ingredient.count():
            pizzas.append(pizza)
    ser = PizzaSerializer(pizzas, many=True)
    return JSONResponse(ser.data)
    # return JSONResponse(PizzaSerializer(list(filter(lambda pizza: pizza.ingredient.filter(expense__amount__lte=F('count')).count() == pizza.ingredient.count(), Pizza.objects.all())), many=True).data)


@auth
def get_order(request):
    instance = Order.objects.get(session=request.session.session_key)
    ser = OrderSerializer(instance)
    return JSONResponse(ser.data)


@auth
def index(request):
    return render(request, 'todo/index.html')


@auth
def add_pizza(request, id):
    order = Order.objects.get(session=request.session.session_key)
    pizza = Pizza.objects.get(pk=id)
    try:
        order_to_pizza = OrderToPizza.objects.get(order=order, pizza=pizza)
        order_to_pizza.amount += 1
    except OrderToPizza.DoesNotExist:
        order_to_pizza = OrderToPizza(order=order, pizza=pizza, amount=1)
    order_to_pizza.save()
    order.price = F('price') + pizza.price
    order.save()
    order = Order.objects.get(pk=order.pk)
    ser = OrderSerializer(order)
    return JSONResponse(ser.data)


@auth
def remove_pizza(request, id):
    order = Order.objects.get(session=request.session.session_key)
    pizza = Pizza.objects.get(pk=id)
    order_to_pizza = OrderToPizza.objects.get(order=order, pizza=pizza)
    if order_to_pizza.amount <= 1:
        order_to_pizza.delete()
    else:
        order_to_pizza.amount -= 1
        order_to_pizza.save()
    order.price = F('price') - pizza.price
    order.save()
    order = Order.objects.get(pk=order.pk)
    ser = OrderSerializer(order)
    return JSONResponse(ser.data)


@auth
def checkout(request):
    order = Order.objects.get(session=request.session.session_key)

    for order_to_pizza in OrderToPizza.objects.filter(order=order):
        pizza = order_to_pizza.pizza
        if pizza.ingredient.filter(
                count__gte=F('expense__amount') * order_to_pizza.amount).count() != pizza.ingredient.count():
            return HttpResponse(status=status.HTTP_507_INSUFFICIENT_STORAGE)

    order.address = json.loads(request.body.decode('UTF-8'))['address']
    order.publish_date = timezone.now()
    order.status = Order.PROCESSING
    order.save()
    ser = OrderSerializer(order)
    return JSONResponse(ser.data)


@auth
def drop_order(request):
    request.session.create()
    order = Order(session=request.session.create_model_instance([]))
    order.save()
    ser = OrderSerializer(order)
    return JSONResponse(ser.data)
