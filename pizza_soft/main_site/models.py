from django.contrib.sessions.models import Session
from django.db import models


class Ingredient(models.Model):
    GRAM = 0
    PIECE = 1
    LITER = 2

    COUNT_TYPES = (
        (GRAM, 'gram'),
        (PIECE, 'piece'),
        (LITER, 'liter'),
    )

    name = models.CharField(max_length=255)
    count = models.DecimalField(max_digits=13, decimal_places=3)
    count_type = models.IntegerField(choices=COUNT_TYPES)

    def __str__(self):
        return self.name


class Pizza(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    ingredient = models.ManyToManyField(Ingredient, through='Expense')
    cooking_time = models.DurationField()
    image = models.ImageField(upload_to='static/images')

    def __str__(self):
        return self.name


class Expense(models.Model):
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=13, decimal_places=3)

    def __str__(self):
        return self.pizza.name + ' have ' + self.ingredient.name


class Order(models.Model):
    NEW = 0
    PROCESSING = 1
    DELIVERED = 2

    STATUSES = (
        (NEW, 'new'),
        (PROCESSING, 'processing'),
        (DELIVERED, 'delivered'),
    )

    session = models.ForeignKey(Session)
    creation_date = models.DateTimeField(auto_now_add=True)
    publish_date = models.DateTimeField(null=True)
    deliver_date = models.DateTimeField(null=True)
    address = models.CharField(max_length=500)
    price = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    status = models.IntegerField(default=NEW, choices=STATUSES)
    pizza = models.ManyToManyField(Pizza, through='OrderToPizza')


class OrderToPizza(models.Model):
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    amount = models.IntegerField()

    class Meta:
        unique_together = ('pizza', 'order',)
