from django.apps import AppConfig


class TodorestConfig(AppConfig):
    name = 'main_site'
