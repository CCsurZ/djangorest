import React, { Component } from 'react'
import ReactDOM from 'react-dom'

class PizzaApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            order: this.props.order,
        };
        this.removePizza = this.removePizza.bind(this);
        this.addPizza = this.addPizza.bind(this);
        this.dropOrder = this.dropOrder.bind(this);
        this.checkOut = this.checkOut.bind(this);
    }
    
    render() {
        return (
            <div className="row">
                <PizzaList pizza={this.props.pizza} addPizza={this.addPizza}/>
                <Order order={this.state.order} onRemovePizza={this.removePizza} onDropOrder={this.dropOrder} onCheckout={this.checkOut}/>
            </div>
        )
    }
    
    removePizza(id) {  
        fetch('api/removepizza/'+id,{credentials: 'same-origin'})
        .then(raw => raw.json())
        .then(parsed => this.setState({order: parsed}));
        /*this.setState(prevstate => {
            for(var i = 0; i < prevstate.order.pizza.length; i++) {
                if(prevstate.order.pizza[i].id === id) {
                   if(prevstate.order.pizza[i].amount == 1) {
                       prevstate.order.pizza.splice(i,1);
                   } else {
                       prevstate.order.pizza[i].amount -= 1;
                   }
                   break;
                }
            }
            return prevstate;
        });*/
    }
    
    addPizza(id) {
        fetch('api/addpizza/'+id,{credentials: 'same-origin'})
        .then(raw => raw.json())
        .then(parsed => this.setState({order: parsed}));
    }
    
    dropOrder() {
        fetch('api/droporder/',{credentials: 'same-origin'})
        .then(raw => raw.json())
        .then(parsed => this.setState({order: parsed}));
    }
    
    checkOut(address) {
        fetch('api/checkout/',{
            method: 'post',
            credentials: 'same-origin',
            body: JSON.stringify({
                address: address
            }),
        }).then(res => res.json()).then(parsed => {
            this.setState({order: parsed});
            alert("Checkout successfully");
        }).catch(() => alert("Cannot checkout"));
    }
}

class Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            address: this.props.order.address,
        };
        this.handleChange = this.handleChange.bind(this);
        this.dropOrder = this.dropOrder.bind(this);
    }
    render(){
        return (
            <div className="col-sm-3">
                <div className="panel panel-primary">
                    <div className="panel-heading clearfix">
                        <div className="panel-title pull-left"><h2 className="custom-h">Order</h2></div>
                        <div className="btn-group pull-right">
                            {this.props.order.status === 0 &&
                                 <button className="btn btn-success" onClick={() => this.props.onCheckout(this.state.address)}>Checkout</button>
                             }
                            <button className={this.props.order.status === 0 ? 'btn btn-danger' : 'btn btn-success'} onClick={this.dropOrder}>{this.props.order.status === 0 ? 'Clear' : 'New'}</button>
                        </div>
                    </div>
                    <div className="panel-body">
                        <label htmlFor="address">Address: </label>
                        <input className="form-control" id="address" onChange={this.handleChange} type="text" value={this.state.address}/>
                        {this.props.order.pizza.length > 0 && 
                            <OrderedPizza pizza={this.props.order.pizza} status={this.props.order.status} totalPrice={this.props.order.price} onRemovePizza={this.props.onRemovePizza} />
                        }
                    </div>
                </div>
            </div>
        )
    }
    handleChange(e) {
        this.setState({address: e.target.value});
    }
    dropOrder(){
        this.setState({
            address: '',
        });
        this.props.onDropOrder();
    }
}

const OrderedPizza = props => (
    <table className="table">
        <thead>
            <tr><th className="text-center" colSpan={props.status === 0 ? 5 : 4}>Ordered pizza</th></tr>
            <tr>
                <th>#</th><th>Name</th><th>Price</th><th>Amount</th>{props.status === 0 && <th></th>}
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colSpan="2">Total price:</td><td colSpan={props.status === 0 ? 3 : 2}>{props.totalPrice}</td>
            </tr>
        </tfoot>
        <tbody>
            {props.pizza.map((pizza, id) => (
                <tr key={pizza.id}>
                    <td>{id+1}</td>
                    <td>{pizza.name}</td>
                    <td>{pizza.price * pizza.amount}</td>
                    <td>{pizza.amount}</td>
                    {props.status === 0 &&
                        <td><button onClick={() => props.onRemovePizza(pizza.id)} className="btn btn-default">Remove</button></td>
                    }
                </tr>
            ))}
        </tbody>
    </table>
);

const PizzaList = props => (
    <div className="col-sm-9 list-group">
        {props.pizza.map(pizza => (
            <Pizza key={pizza.id} pizza={pizza} addPizza={props.addPizza} />
        ))}
    </div>
);

const Pizza = props => (
    <div className="row list-group-item">
        <div className="col-sm-2">
            <img className="img-responsive" src={props.pizza.image}/>
        </div>
        <div className="col-sm-8">
            Name: {props.pizza.name}<br/>
            Cooking time: {props.pizza.cooking_time}<br/>
            Price: {props.pizza.price}<br/>
            Ingredients:
            <ul>
                {props.pizza.ingredient.map(ingredient => (
                    <li key={ingredient.id}> {ingredient.name} </li>
                ))}
            </ul>
        <button onClick={() => props.addPizza(props.pizza.id)} className="btn btn-default">Add pizza</button>
        </div>
    </div>
);

let fetches = [
    fetch('api/order/',{credentials: 'same-origin'}),
    fetch('api/pizza/',{credentials: 'same-origin'}),
];

Promise.all(fetches)
.then(responses => Promise.all(responses.map(res => res.json())))
.then(parsed => ReactDOM.render(<PizzaApp pizza={parsed[1]} order={parsed[0]} />, root));
